/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once


namespace cruft::cxx {
    ///////////////////////////////////////////////////////////////////////////
    template <typename T, T Value>
    struct integral_constant {
        static constexpr T value = Value;
    };


    //-------------------------------------------------------------------------
    struct true_type  : integral_constant<bool,true>  {};
    struct false_type : integral_constant<bool,false> {};


    //-------------------------------------------------------------------------
    namespace detail {
        template <bool ...Values>
        struct _or : integral_constant<bool, (Values || ...)> {};

        template <bool ...Values>
        struct _and : integral_constant<bool, (Values && ...)> {};
    };

    ///////////////////////////////////////////////////////////////////////////
    template <typename, typename> struct is_same : false_type {};
    template <typename T> struct is_same<T,T> : true_type {};
    template <typename A, typename B>
    constexpr auto is_same_v = is_same<A,B>::value;


    ///////////////////////////////////////////////////////////////////////////
    template <typename T> struct remove_const          { using type = T; };
    template <typename T> struct remove_const<const T> { using type = T; };
    template <typename T> using remove_const_t = typename remove_const<T>::type;


    //-------------------------------------------------------------------------
    template <typename T> struct remove_volatile             { using type = T; };
    template <typename T> struct remove_volatile<volatile T> { using type = T; };
    template <typename T> using remove_volatile_t = typename remove_volatile<T>::type;


    //-------------------------------------------------------------------------
    template <typename T>
    struct remove_cv {
        using type = remove_volatile_t<remove_const_t<T>>;
    };

    template <typename T>
    using remove_cv_t = typename remove_cv<T>::type;


    ///////////////////////////////////////////////////////////////////////////
    namespace detail {
        template <typename T>
        struct _is_integral : false_type {};

        template <> struct _is_integral<bool> : true_type {};

        template <> struct _is_integral<short> : true_type {};
        template <> struct _is_integral<int> : true_type {};
        template <> struct _is_integral<long> : true_type {};
        template <> struct _is_integral<long long> : true_type {};

        template <> struct _is_integral<unsigned short> : true_type {};
        template <> struct _is_integral<unsigned int> : true_type {};
        template <> struct _is_integral<unsigned long> : true_type {};
        template <> struct _is_integral<unsigned long long> : true_type {};


        template <> struct _is_integral<char> : true_type {};
        template <> struct _is_integral<unsigned char> : true_type {};
        template <> struct _is_integral<signed char> : true_type {};

#if __cpp_char8_t
        template <> struct _is_integral<char8_t> : true_type {};
#endif
        template <> struct _is_integral<char16_t> : true_type {};
        template <> struct _is_integral<char32_t> : true_type {};
        template <> struct _is_integral<wchar_t> : true_type {};
    };


    //-------------------------------------------------------------------------
    template <typename T>
    struct is_integral : detail::_is_integral<remove_cv_t<T>> {};


    //-------------------------------------------------------------------------
    template <typename T>
    constexpr auto is_integral_v = is_integral<T>::value;


    ///////////////////////////////////////////////////////////////////////////
    namespace detail {
        template <typename T> struct _is_floating_point : false_type {};
        template <> struct _is_floating_point<float> : true_type {};
        template <> struct _is_floating_point<double> : true_type {};
        template <> struct _is_floating_point<long double> : true_type {};
    };


    //-------------------------------------------------------------------------
    template <typename T>
    struct is_floating_point : detail::_is_floating_point<remove_cv_t<T>> {};


    //-------------------------------------------------------------------------
    template <typename T>
    constexpr auto is_floating_point_v = is_floating_point<T>::value;


    ///////////////////////////////////////////////////////////////////////////
    template <typename T>
    struct is_arithmetic : detail::_or<
        is_integral_v<T>,
        is_floating_point_v<T>
    > {};


    //-------------------------------------------------------------------------
    template <typename T>
    constexpr auto is_arithmetic_v = is_arithmetic<T>::value;


    ///////////////////////////////////////////////////////////////////////////
    template <typename T>
    struct is_fundamental : detail::_or<
        is_arithmetic_v<T>,
        is_same_v<T, void>,
        is_same_v<T, decltype(nullptr)>
    > {};


    ///////////////////////////////////////////////////////////////////////////
    namespace detail {
        template <
            typename T,
            bool = is_arithmetic_v<T>
        > struct _is_signed : false_type {};


        template <typename T>
        struct _is_signed<T,true> : integral_constant<bool, T(-1) < T(0)> {};
    };


    //-------------------------------------------------------------------------
    template <typename T>
    struct is_signed : detail::_is_signed<T> {};


    //-------------------------------------------------------------------------
    template <typename T>
    constexpr auto is_signed_v = is_signed<T>::value;


    ///////////////////////////////////////////////////////////////////////////
    template <typename T>
    struct is_unsigned : detail::_and<
        is_arithmetic_v<T>,
        !is_signed_v<T>
    > {};


    //-------------------------------------------------------------------------
    template <typename T>
    constexpr auto is_unsigned_v = is_unsigned<T>::value;
};
