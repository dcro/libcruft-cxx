###############################################################################
cmake_minimum_required(VERSION 3.7.0)
project(cruft-cxx CXX)


###############################################################################
list (APPEND sources
    src/type_traits
    src/exception
    src/exception.cpp
)


##-----------------------------------------------------------------------------
add_library (cruft-cxx STATIC ${sources})


###############################################################################
option (TESTS "enable unit testing" ON)


if (TESTS)
    include (CTest)
    enable_testing ()

    foreach (t type_traits exception)
        string(REPLACE "/" "_" name "test/${t}")
        add_executable(cxx_${name} test/${t}.cpp)
        target_link_libraries(cxx_${name} PRIVATE cruft-cxx cruft)
        target_include_directories(cxx_${name} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
        add_test(NAME cxx_${name} COMMAND cxx_${name})
    endforeach()
endif ()


###############################################################################
configure_file(libcruft-cxx.pc.in libcruft-cxx.pc)
configure_file(Doxyfile.in Doxyfile)
