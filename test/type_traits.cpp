
#include "src/type_traits"


int main ()
{
    using namespace cruft::cxx;

    // Type comparisons
    static_assert (is_same_v<int,int>);
    static_assert (!is_same_v<int,bool>);
    static_assert (!is_same_v<int,int const>);
    static_assert (!is_same_v<int&,int>);

    // CV removal
    static_assert (is_same_v<remove_const<int const>::type, remove_const_t<int const>>);

    static_assert (
        is_same_v<
            int,
            typename remove_const<int const>::type
        >
    );

    static_assert (is_same_v<int, remove_volatile_t<int volatile>>);

    static_assert (is_same_v<remove_cv_t<int const volatile>, int>);

    // Integral tests
    static_assert (is_integral_v<int>);
    static_assert (is_integral_v<char>);
    static_assert (is_integral_v<bool>);

    static_assert (is_integral_v<unsigned const volatile>);

    // Floating point tests
    static_assert (is_floating_point_v<float>);
    static_assert (is_floating_point_v<double>);
    static_assert (is_floating_point_v<long double>);
    static_assert (!is_floating_point_v<char>);
    static_assert (!is_floating_point_v<bool>);

    // Arithmetic tests
    static_assert (is_arithmetic_v<float>);
    static_assert (is_arithmetic_v<unsigned>);
    static_assert (!is_arithmetic_v<decltype(main)>);

    // Signedness
    static_assert (is_signed_v<signed>);
    static_assert (!is_signed_v<unsigned>);
    static_assert (is_signed_v<float>);
    static_assert (!is_signed_v<decltype(main)>);

    static_assert (is_unsigned_v<unsigned>);
    static_assert (!is_unsigned_v<signed>);
    static_assert (!is_unsigned_v<float>);
    static_assert (!is_unsigned_v<decltype(main)>);
}
